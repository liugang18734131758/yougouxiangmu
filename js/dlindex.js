//二维码移入移出事件
$('.header .left-content>li').hover(
    function () {
      var $code = $(this).find('div.scan-code>div.border-content')
      $code.stop(true, true).show()
    },
    function () {
      var $code = $(this).find('div.scan-code>div.border-content')
      $code.stop(true, true).hide()
    }
  )
  // 公告
$('div.notice').hover(
    function () {
      var $notice = $(this).find('.notice-content')
      $notice.stop(true, true).show()
    },
    function () {
      var $notice = $(this).find('.notice-content')
      $notice.stop(true, true).hide()
    }
  )