//二维码移入移出事件
$('.header .left-content>li').hover(
    function () {
      var $code = $(this).find('div.scan-code>div.border-content')
      $code.stop(true, true).show()
    },
    function () {
      var $code = $(this).find('div.scan-code>div.border-content')
      $code.stop(true, true).hide()
    }
  )
  // 菜单移入移出事件
  var menuList = {}
  // var currentOffet = 0
  $('[_yg_nav]').hover(
    function () {
      // 获取logo区域整体高度
      var $logoHeight = $('#logo').height()
      $('.nav-container-down').css('top', $logoHeight)
  
      var _nav = $(this).attr('_yg_nav')
  
      // 获取当前移入的菜单位置
      // if($(this)[0].tagName === 'A'){
      //   currentOffet = $(this).offset().left;
      // }
      // // 子菜单与顶级菜单左边对齐
      // $('#' + _nav).find('.sub-container').css('margin-left', currentOffet);
  
      var currentOffet = $('#nav_logo').next().offset().left + 15;
      $('#' + _nav)
        .find('.sub-container')
        .css('margin-left', currentOffet)
  
      clearTimeout(menuList[_nav + '_timer'])
  
      menuList[_nav + '_timer'] = setTimeout(function () {
        $('[_yg_nav]').each(function () {
          $(this)[
            _nav == $(this).attr('_yg_nav')
              ? 'addClass'
              : 'removeClass'
          ]('nav-up-selected')
        })
  
        $('#' + _nav).stop(true, true).slideDown(200)
      }, 150)
    },
    function () {
      var _nav = $(this).attr('_yg_nav')
  
      clearTimeout(menuList[_nav + '_timer'])
  
      menuList[_nav + '_timer'] = setTimeout(function () {
        $('[_yg_nav]').removeClass('nav-up-selected')
  
        $('#' + _nav).stop(true, true).slideUp(200)
      }, 150)
    }
  )
  // 公告
  $('div.notice').hover(
    function () {
      var $notice = $(this).find('.notice-content')
      $notice.stop(true, true).show()
    },
    function () {
      var $notice = $(this).find('.notice-content')
      $notice.stop(true, true).hide()
    }
  )
  // 搜索
  $('.search_btn').bind('click', function (e) {
    e.stopPropagation()
    var keyword = $('#keyword').val()
    if (!keyword == '') {
      $(this).parents('form').submit()
    }
  })
  // 放大镜
  window.onload = function() {
    var oDiv = document.getElementById("div1");
    var oMark = document.getElementsByClassName("mark")[0];
    var oFloat = document.getElementsByClassName("float_layer")[0];
    var oBig = document.getElementsByClassName("big_pic")[0];
    var oSmall = document.getElementsByClassName("small_pic")[0];
    var oImg = oBig.getElementsByTagName("img")[0];
    //给遮罩层添加鼠标移入事件
    oMark.onmouseover = function() {
        oFloat.style.display = 'block';
        oBig.style.display = 'block';
    };
    //给遮罩层添加鼠标移出事件
    oMark.onmouseout = function() {
        oFloat.style.display = "none";
        oBig.style.display = "none";
    };
    //给遮罩层添加鼠标移动事件
    oMark.onmousemove = function(evt) {
        var e = evt || window.event;
        document.title = e.clientX - oDiv.offsetLeft - oSmall.offsetLeft;
        var l = e.clientX - oDiv.offsetLeft - oSmall.offsetLeft - oFloat.offsetWidth / 2;
        var t = e.clientY - oDiv.offsetTop - oSmall.offsetTop - oFloat.offsetHeight / 2;
        if (l < 0) {
            l = 0;
        } else if (l > oMark.offsetWidth - oFloat.offsetWidth) {
            l = oMark.offsetWidth - oFloat.offsetWidth;
        }
        if (t < 0) {
            t = 0;
        } else if (t > oMark.offsetHeight - oFloat.offsetHeight) {
            t = oMark.offsetHeight - oFloat.offsetHeight;
        }
        oFloat.style.left = l + 'px';
        oFloat.style.top = t + 'px';
        //小块活动的距离 （移动比例）大图显示比例
        var percentX = l / (oMark.offsetWidth - oFloat.offsetWidth);
        var percentY = t / (oMark.offsetHeight - oFloat.offsetHeight);
        //大图的left值 = 移动比例 * （大图的宽度 - 大图所在Div的宽度）大图所能移动的距离
        oImg.style.left = -percentX * (oImg.offsetWidth - oBig.offsetWidth) + 'px';
        oImg.style.top = -percentY * (oImg.offsetHeight - oBig.offsetHeight) + 'px';
    };
};