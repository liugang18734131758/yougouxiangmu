window.onload = function() {
    var aData = [{
            "imgUrl": "https://i2.ygimg.cn/pics/belle/2019/101303691/101303691_01_t.jpg?8",
            "proName": "羊头",
            "proPrice": "179",
            "proComm": "18"
        },
        {
            "imgUrl": "https://i1.ygimg.cn/pics/belle/2020/101307035/101307035_02_t.jpg?3",
            "proName": "BELLE/百丽2020春新商场同款复合材料/纺织品女老爹鞋",
            "proPrice": "149",
            "proComm": "1.4"
        },
        {
            "imgUrl": "https://i2.ygimg.cn/pics/belle/2019/101306760/101306760_01_t.jpg?3",
            "proName": "时尚女鞋",
            "proPrice": "199",
            "proComm": "4.2"
        },
        {
            "imgUrl": "https://i2.ygimg.cn/pics/belle/2020/101305382/101305382_01_t.jpg?3/cs/500x500d.png",
            "proName": "BELLE/百丽2020春新商场同款绣花布女乐福鞋皮鞋",
            "proPrice": "129",
            "proComm": "3.6"
        },
    ];
    var oBox = document.getElementById("box");
    var oCar = document.getElementById("car");
    var oUl = document.getElementsByTagName("ul")[0];
    for (var i = 0; i < aData.length; i++) {
        var oLi = document.createElement("li");
        var data = aData[i];
        oLi.innerHTML += '<div class="pro_img"><img src="' + data["imgUrl"] + '" width="150" height="150"></div>';
        oLi.innerHTML += '<h3 class="pro_name"><a href="#">' + data["proName"] + '</a></h3>';
        oLi.innerHTML += '<p class="pro_price">' + data["proPrice"] + '元</p>';
        oLi.innerHTML += '<p class="pro_rank">' + data["proComm"] + '万人评价</p>';
        oLi.innerHTML += '<div class="add_btn">加入购物车</div>';
        oUl.appendChild(oLi);
    }
    var aBtn = getClass(oBox, "add_btn");
    for (var i = 0; i < aBtn.length; i++) {
        aBtn[i].index = i;
        aBtn[i].onclick = function() {
            var oDiv = document.createElement("div");
            var data = aData[this.index];
            oDiv.className = "row hid";
            oDiv.innerHTML += '<div class="img left"><img src="' + data["imgUrl"] + '" width="80" height="80"></div>';
            oDiv.innerHTML += '<div class="name left"><span>' + data["proName"] + '</span></div>';
            oDiv.innerHTML += '<div class="price left"><span>' + data["proPrice"] + '元</span></div>';
            oDiv.innerHTML += '<div class="ctrl left"><a href="javascript:;">×</a></div>';
            oCar.appendChild(oDiv);
            var delBtn = oDiv.lastChild.getElementsByTagName("a")[0];
            delBtn.onclick = function() {
                oCar.removeChild(oDiv);
            }
        }
    }

    function getClass(oBox, tagname) {
        var aTag = oBox.getElementsByTagName("*");
        var aBox = [];
        for (var i = 0; i < aTag.length; i++) {
            if (aTag[i].className == tagname) {
                aBox.push(aTag[i]);
            }
        }
        return aBox
    }
}