//二维码移入移出事件
$('.header .left-content>li').hover(
    function () {
      var $code = $(this).find('div.scan-code>div.border-content')
      $code.stop(true, true).show()
    },
    function () {
      var $code = $(this).find('div.scan-code>div.border-content')
      $code.stop(true, true).hide()
    }
  )
  // 菜单移入移出事件
  var menuList = {}
  // var currentOffet = 0
  $('[_yg_nav]').hover(
    function () {
      // 获取logo区域整体高度
      var $logoHeight = $('#logo').height()
      $('.nav-container-down').css('top', $logoHeight)
  
      var _nav = $(this).attr('_yg_nav')
  
      // 获取当前移入的菜单位置
      // if($(this)[0].tagName === 'A'){
      //   currentOffet = $(this).offset().left;
      // }
      // // 子菜单与顶级菜单左边对齐
      // $('#' + _nav).find('.sub-container').css('margin-left', currentOffet);
  
      var currentOffet = $('#nav_logo').next().offset().left + 15;
      $('#' + _nav)
        .find('.sub-container')
        .css('margin-left', currentOffet)
  
      clearTimeout(menuList[_nav + '_timer'])
  
      menuList[_nav + '_timer'] = setTimeout(function () {
        $('[_yg_nav]').each(function () {
          $(this)[
            _nav == $(this).attr('_yg_nav')
              ? 'addClass'
              : 'removeClass'
          ]('nav-up-selected')
        })
  
        $('#' + _nav).stop(true, true).slideDown(200)
      }, 150)
    },
    function () {
      var _nav = $(this).attr('_yg_nav')
  
      clearTimeout(menuList[_nav + '_timer'])
  
      menuList[_nav + '_timer'] = setTimeout(function () {
        $('[_yg_nav]').removeClass('nav-up-selected')
  
        $('#' + _nav).stop(true, true).slideUp(200)
      }, 150)
    }
  )
  // 公告
  $('div.notice').hover(
    function () {
      var $notice = $(this).find('.notice-content')
      $notice.stop(true, true).show()
    },
    function () {
      var $notice = $(this).find('.notice-content')
      $notice.stop(true, true).hide()
    }
  )
  // 搜索
  $('.search_btn').bind('click', function (e) {
    e.stopPropagation()
    var keyword = $('#keyword').val()
    if (!keyword == '') {
      $(this).parents('form').submit()
    }
  })