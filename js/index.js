//二维码移入移出事件
$('.header .left-content>li').hover(
  function () {
    var $code = $(this).find('div.scan-code>div.border-content')
    $code.stop(true, true).show()
  },
  function () {
    var $code = $(this).find('div.scan-code>div.border-content')
    $code.stop(true, true).hide()
  }
)
// 菜单移入移出事件
var menuList = {}
// var currentOffet = 0
$('[_yg_nav]').hover(
  function () {
    // 获取logo区域整体高度
    var $logoHeight = $('#logo').height()
    $('.nav-container-down').css('top', $logoHeight)

    var _nav = $(this).attr('_yg_nav')

    // 获取当前移入的菜单位置
    // if($(this)[0].tagName === 'A'){
    //   currentOffet = $(this).offset().left;
    // }
    // // 子菜单与顶级菜单左边对齐
    // $('#' + _nav).find('.sub-container').css('margin-left', currentOffet);

    var currentOffet = $('#nav_logo').next().offset().left + 15;
    $('#' + _nav)
      .find('.sub-container')
      .css('margin-left', currentOffet)

    clearTimeout(menuList[_nav + '_timer'])

    menuList[_nav + '_timer'] = setTimeout(function () {
      $('[_yg_nav]').each(function () {
        $(this)[
          _nav == $(this).attr('_yg_nav')
            ? 'addClass'
            : 'removeClass'
        ]('nav-up-selected')
      })

      $('#' + _nav).stop(true, true).slideDown(200)
    }, 150)
  },
  function () {
    var _nav = $(this).attr('_yg_nav')

    clearTimeout(menuList[_nav + '_timer'])

    menuList[_nav + '_timer'] = setTimeout(function () {
      $('[_yg_nav]').removeClass('nav-up-selected')

      $('#' + _nav).stop(true, true).slideUp(200)
    }, 150)
  }
)
// 公告
$('div.notice').hover(
  function () {
    var $notice = $(this).find('.notice-content')
    $notice.stop(true, true).show()
  },
  function () {
    var $notice = $(this).find('.notice-content')
    $notice.stop(true, true).hide()
  }
)
// 搜索
$('.search_btn').bind('click', function (e) {
  e.stopPropagation()
  var keyword = $('#keyword').val()
  if (!keyword == '') {
    $(this).parents('form').submit()
  }
})

// 鼠标hover出现品牌logo
$('ul.brand-pictures > li > a').hover(
  function (e) {
    $(this).children('div').removeClass('hide')
  },
  function () {
    $(this).children('div').addClass('hide')
  }
)
// 左右箭头
 var len = $(".swiper-item-wrap li").length/4;
var i = 0;
$(".arrow-left").click(function () {
  i++;
  if (i >= len) {
    i = 0;
  }
  console.log(i);
  $(".swiper-item-wrap").animate({
    "margin-left": -i * 300 + "px"
  }, 500, function () { })
})

$(".arrow-right").click(function () {
  i--;
  if (i <= 0) {
    i = len - 1;
  }
  $(".swiper-item-wrap").animate({
    "margin-left": -i * 300 + "px"
  }, 500, function () { })
})
//按键轮播
var num = setInterval("paly()", 3000);

function paly() {
    var index = Math.floor(Math.random() * 5);
    $("ul li").eq(index).show();
    $("ul li").eq(index).siblings().hide();
    $("#xh span:eq(" + index + ")").css("background", "yellow"),
        $("#xh span:eq(" + index + ")").siblings().css("background", "white");
}
$(document).ready(function(e) {
    //
    $("div span").each(function(index, element) {
        $(this).mouseover(function() {
            $(this).css("background-color", "yellow");
            $("ul li").eq(index).show();
            $("ul li").eq(index).siblings().hide(); //遍历前后所有的同辈元素
        }).mouseout(function() {
            $(this).css("background-color", "white");
        });
    });
});